MAVEN IMAGE
-----------

That image contains default docker maven image with specific customizations:

1. Added [settings.xml](settings.xml) with properties for maven artifact storage and docker registry
2. Added `settings-security.xml` file with encrypted master-password from project CI variable-file for
    decrypting passwords in `settings.xml`
3. Added docker-credential-helper-pass to provide secure storage for docker secrets. Used linux pass with gpg
    encryption. [Steps to reproduce all required dependencies](docs/docker-credential-helper-pass.md).
    It is required for [testcontainers](https://www.testcontainers.org) usage

Required Gitlab CI properties for build:  

| property                |   type      | description                                          |
|:------------------------|:------------|:-----------------------------------------------------|   
| `GITLAB_HOST`           | variable    |  gitlab server host                                  |
| `GIT_USER_EMAIL`        | variable    |  git user email with access to pass storage git repo |
| `GIT_USER_NAME`         | variable    |  git user name with access to pass storage git repo  |
| `PASS_STORAGE_GIT_REPO` | variable    |  pass storage git repo in `.git` format              |
| `GITLAB_SSH_PRIVATE_KEY`| file        |  SSH private key for registered in Gitlab public key |
| `GPG_PRIVATE_KEY`       | file        |  GPG private key to decrypt pass storage content     |
| `GPG_OWNERTRUST`        | file        |  GPG ownertrust file                                 |
| `MVN_SETTINGS_SECURITY` | file        |  maven `settings-security.xml` file with master password |

Steps to prepare all required dependencies for docker-credential-helper
=======================================================================

1. Generate GPG keys:
    - install gpg and rng-tools:
    ```
        apt-get update \
            && apt-get install -y gpg rng-tools
    ```

   - register rng-tools in random:
   ```
        rngd -r /dev/urandom
   ```

   - create file `gen-key-skript` with properties for generating GPG keys, that contains
      (change `gpg_key_name` and `gpg_key_email`):
   ```
        %no-protection
        Key-Type: 1
        Key-Length: 2048
        Subkey-Type: 1
        Subkey-Length: 2048
        Name-Real: <gpg_key_name>
        Name-Email: <gpg_key_email>
        Expire-Date: 0
   ```

   - generate GPG keys:
   ```
        gpg --batch --full-generate-key gen-key-script
   ```

   - export GPG keys to files and save them for next steps (change `gpg_key_email`):
   ```
       1) gpg -a --export <gpg_key_email> > public-gpg.key
       2) gpg -a --export-secret-keys <gpg_key_email> > secret-gpg.key
       3) gpg --export-ownertrust > ownertrust.txt
   ```

2. Install and configure pass:
    - install dependencies:
   ```
        apt-get update \
            && apt-get install -y pass openssh-client git
   ```

   - get GPG key id with command (should be like `4FD5F8DA8D4DB9777DB3BCD4E370F70E57944DE4`):
   ```
        gpg --list-keys
   ```

   - initiate pass store (use your key):
   ```
        pass init 4FD5F8DA8D4DB9777DB3BCD4E370F70E57944DE4
   ```

   - generate SSH keys to have access for GitLab with git (or get existing one) and save them for next steps:
   ```
        ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
   ```

   - register SSH keys and add GitLab to known hosts (set `gitlab_host`):
   ```
        eval $(ssh-agent -s) \
            && ssh-add ~/.ssh/gitlab_key \
            && ssh-keyscan -t rsa <gitlab_host> >> ~/.ssh/known_hosts
   ```

   - add SSH public key to Gitlab user account. User -> Settings -> SSH Keys -> Paste public key, add title and save.

   - configure git and pass git with gitlab pass storage repo (set `git_user_email`, `git_user_name`, `pass_storage_git_repos`):
   ```
        git config --global user.email <git_user_email> \
            && git config --global user.name <git_user_name> \
            && pass git init \
            && pass git remote add origin <pass_storage_git_repo>
   ```

3. Install docker-credential-helper-pass and add docker credentials
    - install docker-credential-helper pass:
   ```
        mkdir ~/bin; cd ~/bin \
            && echo 'export PATH=$PATH:~/bin' >> ~/.bashrc \
            && source ~/.bashrc \
            && wget https://github.com/docker/docker-credential-helpers/releases/download/v0.6.3/docker-credential-pass-v0.6.3-amd64.tar.gz \
            && tar xvzf docker-credential-pass-v0.6.3-amd64.tar.gz \
            && chmod a+x docker-credential-pass \
            && mkdir ~/.docker \
            && echo '{ "credsStore": "pass" }' > ~/.docker/config.json
   ```

   - configure pass with docker-credential-helper-pass (leave passphrase empty):
   ```
        pass insert docker-credential-helpers/docker-pass-initialized-check
   ```

   - install docker with official tutorial: https://docs.docker.com/engine/install/ubuntu/

   - login to docker with docker credentials (set `registry_username`, `registry_password`, `docker_registry`):
   ```
        docker login -u <registry_username> -p <registry_password> <docker_registry>
   ```

4. Push pass storage to GitLab
    - push to GitLab:
    ```
        pass git push --set-upstream origin master
    ```

5. Add SSH and GPG keys to CI/CD variables:
    - add variable `GITLAB_SSH_PRIVATE_KEY` with type `File` and contents of generated SSH private key on previous stages

    - add variable `GITLAB_SSH_PUBLIC_KEY` with type `File` and contents of generated SSH public key on previous stages

    - add variable `GPG_OWNERTRUST` with type `File` and contents of `ownertrust.txt`

    - add variable `GPG_PRIVATE_KEY` with type `File` and contents of `secret-gpg.key`
